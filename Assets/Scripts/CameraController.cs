﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public PlayerController thePlayer;
	public bool moveCamera = true;
	private Vector3 lastPlayerPosition;
	private float distanceToMove;
	// Use this for initialization
	void Awake () {
        Application.targetFrameRate = 60;
        thePlayer = FindObjectOfType<PlayerController> ();
		lastPlayerPosition = thePlayer.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(moveCamera);
        if (moveCamera)
        {
            distanceToMove = thePlayer.transform.position.x - lastPlayerPosition.x;

            transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);

            lastPlayerPosition = thePlayer.transform.position;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(2);
        }

    }

    public void setMoveCamera(bool var)
    {
        this.moveCamera = var;
    }
}
