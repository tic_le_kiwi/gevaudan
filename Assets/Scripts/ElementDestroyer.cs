﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementDestroyer : MonoBehaviour {

	public GameObject elementDestructionPoint;

	// Use this for initialization
	void Awake () {
		elementDestructionPoint = GameObject.Find("ElementDestructionPoint");
	}
	
	// Update is called once per frame
	void Update () {
		DestroyElements ();
	}

	void DestroyElements() {
		if (transform.position.x < elementDestructionPoint.transform.position.x) {
			Destroy (gameObject);
		}
	}
}
