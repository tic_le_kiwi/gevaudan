﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public GameObject theGround;
    public Transform groundGenerator;
    public static GameManager instance;
    private Vector3 groundStartPoint;

    public Transform platformGenerator;
    private Vector3 platformStartPoint;

    public PlayerController thePlayer;
    private Vector3 playerStartPoint;

    public Text scoreText;
    public static int score;

	public CameraController mainCamera;

    // Use this for initialization
    void Awake()
    {
        groundStartPoint = groundGenerator.position;
        playerStartPoint = thePlayer.transform.position;
        platformStartPoint = platformGenerator.position;
		ResetScore ();
        instance = this;
    }

    private void UpdateScore()
    {
        scoreText.text = score.ToString();
    }

	private void ResetScore(){
		score = 0;
		UpdateScore ();
	}

    public void AddScore(int add)
    {
        Debug.Log(add);
        score += add;
        UpdateScore();
    }

    public int GetScore()
    {
        return score;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RestartPlayer()
    {
        StartCoroutine("RestartGameCo");
    }

    public IEnumerator RestartGameCo()
    {
		mainCamera.moveCamera = false;
		thePlayer.canRun = false;
        yield return new WaitForSeconds(1);
		thePlayer.gameObject.SetActive (false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

	void ResetScene(){
		ElementDestroyer[] elements = FindObjectsOfType(typeof(ElementDestroyer)) as ElementDestroyer[];
		foreach (ElementDestroyer element in elements) {
			Destroy (element.gameObject);
		}
		GenerateFirstGround();
		ResetScore ();
	}
    void GenerateFirstGround()
    {
        Instantiate(theGround, theGround.transform.position, theGround.transform.rotation);
    }
}
