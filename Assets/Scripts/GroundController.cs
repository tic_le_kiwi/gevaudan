﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour {

	public GameObject tree_burton1;
	public GameObject tree_burton2;
	public GameObject tree_burton3;
	public GameObject tree_burton4;

	public GameObject tree_chelou1;
	public GameObject tree_chelou2;
	public GameObject tree_chelou3;

	public GameObject tree_trait1;
	public GameObject tree_trait2;
	public GameObject tree_trait3;
	public GameObject tree_trait4;
	public GameObject tree_trait5;
	public GameObject tree_trait6;
	public GameObject tree_trait7;
	public GameObject tree_trait8;


	public GameObject house1;
	public GameObject house2;
	public GameObject house3;

	public GameObject grave1;
	public GameObject grave2;
	public GameObject grave3;

    private GameManager _gameManager;

	// Use this for initialization
	void Awake ()
	{
	    _gameManager = GameManager.instance;
		ActiveTree ();
		ActiveHouse ();
		ActiveGrave ();
	}

	void ActiveTree()
	{
	    int choiceTypeTree = _gameManager.GetScore() / 20;
        Debug.Log("machin:      " +choiceTypeTree);
        switch (choiceTypeTree) {
			case 0:
				DestroyChelouTrees ();
				DestroyTraitTrees ();
			break;
			case 1:
				DestroyBurtonTrees ();
				DestroyTraitTrees ();
			break;
			case 2:
				DestroyBurtonTrees ();
				DestroyChelouTrees ();
				GenerateRandomTreeTraits ();
			break;
			default: 
				DestroyBurtonTrees ();
				DestroyChelouTrees ();
				DestroyTraitTrees ();
			break;
		}
	}

	void DestroyBurtonTrees(){
		Destroy (tree_burton1);
		Destroy (tree_burton2);
		Destroy (tree_burton3);
		Destroy (tree_burton4);
	}

	void DestroyChelouTrees(){
		Destroy (tree_chelou1);
		Destroy (tree_chelou2);
		Destroy (tree_chelou3);
	}

	void DestroyTraitTrees(){
		Destroy (tree_trait1);
		Destroy (tree_trait2);
		Destroy (tree_trait3);
		Destroy (tree_trait4);
		Destroy (tree_trait5);
		Destroy (tree_trait6);
		Destroy (tree_trait7);
		Destroy (tree_trait8);
	}

	void GenerateRandomTreeTraits(){
		if(RandomBool())
			Destroy (tree_trait1);
		if(RandomBool())
			Destroy (tree_trait2);
		if(RandomBool())
			Destroy (tree_trait3);
		if(RandomBool())
			Destroy (tree_trait4);
		if(RandomBool())
			Destroy (tree_trait5);
		if(RandomBool())
			Destroy (tree_trait6);
		if(RandomBool())
			Destroy (tree_trait7);
		if(RandomBool())
			Destroy (tree_trait8);
	}


	void ActiveHouse(){
		int choiceHouse = Random.Range (1, 8);
		switch (choiceHouse) {
			case 1:
				house1.SetActive (true);
				Destroy (house2);
				Destroy (house3);
				break;
			case 2:
				Destroy (house1);
				house2.SetActive (true);
				Destroy (house3);
				break;
			case 3:
				Destroy (house1);
				Destroy (house2);
				house3.SetActive (true);
				break;
			default: 
				Destroy (house1);
				Destroy (house2);
				Destroy (house3);
				break;
		}
	}

	void ActiveGrave(){
		int choiceGrave = Random.Range (1, 8);
		switch (choiceGrave) {
			case 1:
				grave1.SetActive (true);
				Destroy (grave2);
				Destroy (grave3);
				grave2.SetActive (false);
				break;
			case 2:
				Destroy (grave1);
				grave2.SetActive (true);
				Destroy (grave3);
				break;
			case 3:
				Destroy (grave1);
				Destroy (grave2);
				grave3.SetActive (true);
				break;
			default: 
				Destroy (grave1);
				Destroy (grave2);
				Destroy (grave3);
				break;
		}
	}

	bool RandomBool(){
		return (Random.value > 0.5f); 
	}
		
}
