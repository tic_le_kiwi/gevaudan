﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundGenerator : MonoBehaviour
{

    public GameObject theGround;
    public Transform generationPoint;
    public float distanceBetween;

    private int generationCounter = 0;
    private float groundWidth;

    public GameObject theSpikes;
    private Vector3 spikePosition;

	public GameObject theWolfTrap;
	private Vector3 wolfTrapPosition;

	public GameObject theFireTrap;
	private Vector3 fireTrapPosition;

    // Use this for initialization
    void Awake()
    {
        groundWidth = theGround.GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        GenerateGround();
    }

	void GenerateGround() {
		if (transform.position.x < generationPoint.position.x) {
			float xPosition = transform.position.x + groundWidth + distanceBetween;
			transform.position = new Vector3 (xPosition, theGround.transform.position.y, theGround.transform.position.z);
			Instantiate (theGround, transform.position, transform.rotation);
			generationCounter++;
			if (RandomBool()) {
				int typeTrap = Random.Range (1, 4);
				switch (typeTrap) {
				case 1:
					spikePosition = new Vector3 (xPosition, theSpikes.transform.position.y, theSpikes.transform.position.z);
					Instantiate (theSpikes, spikePosition, transform.rotation);
					break;
				case 2:
					wolfTrapPosition = new Vector3 (xPosition, theWolfTrap.transform.position.y, theWolfTrap.transform.position.z);
					Instantiate (theWolfTrap, wolfTrapPosition, transform.rotation);
					break;
				case 3:
					fireTrapPosition = new Vector3 (xPosition, theFireTrap.transform.position.y, theFireTrap.transform.position.z);
					Instantiate (theFireTrap, fireTrapPosition, transform.rotation);
					break;
				default:
					break;
				}
			}
		}
	}

	bool RandomBool(){
		return (Random.value > 0.5f); 
	}
		
}
