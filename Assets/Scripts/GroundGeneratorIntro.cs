﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundGeneratorIntro : MonoBehaviour
{

    public GameObject theGround;
    public Transform generationPoint;
    public float distanceBetween;

    private int generationCounter = 0;
    private float groundWidth;
    public GameObject theSpikes;
    private float spikesWidth;
    private Vector3 spikePosition;

    // Use this for initialization
    void Start()
    {
        groundWidth = theGround.GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        GenerateGround();
    }

	void GenerateGround() {
		if (transform.position.x < generationPoint.position.x) {
			float xPosition = transform.position.x + groundWidth + distanceBetween;
			transform.position = new Vector3 (xPosition, theGround.transform.position.y, theGround.transform.position.z);
			Instantiate (theGround, transform.position, transform.rotation);
			generationCounter++;
            //if (generationCounter % 3 == 0 && RandomBool())
            //{
            //    spikePosition = new Vector3(xPosition, theSpikes.transform.position.y, transform.position.z);
            //    Instantiate(theSpikes, spikePosition, transform.rotation);
            //}
        }
	}

	bool RandomBool(){
		return (Random.value > 0.5f); 
	}
		
}
