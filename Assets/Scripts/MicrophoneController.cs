﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicrophoneController : MonoBehaviour
{
    private static string _device;
    private static AudioSource _audio;
    private static List<float> _volumes;
    private static int _position;
    private static LineRenderer _line;
    private static List<Vector3> _vectors;
    private static bool _canSpawn;
    private static float _timeUntilSpawn;
    public GameObject Platform;
    public Transform generationPoint;
    // Use this for initialization
    void Awake()
    {
        _line = GetComponent<LineRenderer>();
        _line.material = Resources.Load("BaseMaterial") as Material;
        _line.SetColors(Color.red, Color.red);
        _position = -144;
        _canSpawn = true;
        _vectors = new List<Vector3>();
        _vectors.Add(new Vector3(_position, -2));
        _line.SetPositions(_vectors.ToArray());
        _volumes = new List<float>();
        foreach (var dev in Microphone.devices)
        {
            Debug.Log(dev);
        }
        if (_device == null) _device = Microphone.devices[0];
        if (_audio == null) _audio = GetComponent<AudioSource>();
        _audio.clip = Microphone.Start(MicrophoneController._device, true, 999, 44100);
        while (!(Microphone.GetPosition(_device) > 0))
        { }
        _audio.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GeneratePlatformFromSound(float max)
    {
        if (max > 50)
        {
            _volumes.Add(max);
        }
        else
        {
            if (_volumes.Count > 0)
            {
                //if (_volumes.Count > 2)
                //{
                    _volumes.Sort();
                    GameObject g = Instantiate(Platform, new Vector3(
                        generationPoint.transform.position.x + 8,
                        (float)-3,
                        -2),
                        transform.rotation);
                    StartCoroutine(ScalePlatform(g));
                /*}
                else
                    Debug.Log("Short");*/
                _volumes = new List<float>();
                _canSpawn = false;
                _timeUntilSpawn = (float)1;
            }
        }
    }

    IEnumerator ScalePlatform(GameObject g)
    {

        var startScale = Vector3.one;
        var endScale = new Vector3(1,(float) ((_volumes[_volumes.Count - 1] / 100)*1.2),1);
        var t = 0.0;
        var speed = 0.8;
        while (t < 1.0)
        {
            t += Time.deltaTime * speed;
            g.transform.localScale = Vector3.Lerp(startScale, endScale, (float)t);
            yield return null;
        }
    }

    void FixedUpdate()
    {
        float[] data = new float[735];
        _timeUntilSpawn -= Time.deltaTime;
        if (_timeUntilSpawn <= 0)
            _canSpawn = true;
        float max;
        _audio.GetOutputData(data, 0);
        //take the median of the recorded samples
        ArrayList s = new ArrayList();
        foreach (float f in data)
        {
            s.Add(Mathf.Abs(f));
        }
        s.Sort();
        max = (float)s[734];
        max *= 1000;
        //Debug.Log(max);
        if (_canSpawn)
            GeneratePlatformFromSound(max);
        Vector3 v = new Vector3(_position + 1, (max / 100) - 2);
        _vectors.Add(v);
        _line.SetPositions(_vectors.ToArray());
        _position++;
        if (_vectors.Count >= 88)
        {
            _vectors = new List<Vector3>();
            _position = -144;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (GameObject plat in GameObject.FindGameObjectsWithTag("Platform/Black"))
            {
                Destroy(plat);
            }
            _canSpawn = false;
            _timeUntilSpawn = (float) 0;
        }
    }
}
