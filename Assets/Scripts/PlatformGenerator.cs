﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {


	public GameObject thePlatform;
	public Transform generationPoint;
	public float distanceBetween;

	private float platformWidth;


	// Use this for initialization
	void Awake () {
		platformWidth = thePlatform.GetComponent<BoxCollider2D> ().size.x;
	}

	void Update () {
		//GeneratePlatform ();
	}

	private void GeneratePlatform() {
		if (transform.position.x < generationPoint.position.x) {
			float xPosition = transform.position.x + platformWidth + distanceBetween;
			transform.position = new Vector3 (xPosition, transform.position.y, thePlatform.transform.position.z);
			Instantiate (thePlatform, transform.position, transform.rotation);
		}
	}
		
}
