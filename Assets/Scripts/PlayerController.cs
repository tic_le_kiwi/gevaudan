﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float jumpForce;
	public bool canRun = true;

	private Rigidbody2D myRigidBody;

	public bool grounded;
	public bool isFalling;
	public LayerMask whatIsGround;

    public AudioClip wolfDied;
    public AudioClip wolfHowl1;
    public AudioClip wolfHowl2;
    public AudioClip wolfEat;
    public AudioClip wolfRunning;

    private Collider2D myCollider;
	private float lastPositionX;
    private SpriteRenderer spriteRenderer;

    private Animator myAnimator;

	public GameManager theGameManager;

    public AudioSource source;
    public void Awake()
    {
        source = GetComponent<AudioSource>();
        myRigidBody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
        myAnimator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    void Start () {
		
	}

	void FixedUpdate () {
		if (canRun) {
			grounded = Physics2D.IsTouchingLayers (myCollider, whatIsGround);
			myRigidBody.velocity = new Vector2 (moveSpeed, myRigidBody.velocity.y);

			myAnimator.SetFloat ("Speed", myRigidBody.velocity.x);
			myAnimator.SetBool ("Grounded", grounded);


			isFalling = (myRigidBody.velocity.y < -0.1);
			myAnimator.SetBool ("isFalling", isFalling);
			//Debug.Log ("isFalling");
			//Debug.Log (isFalling);

			//Debug.Log ("Grounded");
			//Debug.Log (grounded);

			DebugRun ();
		}
	}

	void DebugRun(){
		//Debug.Log ("lastPositionX = " + lastPositionX);
		//Debug.Log ("transform.position.x = " + transform.position.x);

		if (lastPositionX == transform.position.x) {
			//Debug.Log ("Bug corrected");
			transform.position = new Vector3 (transform.position.x + 0.1F, transform.position.y, transform.position.z);
			if(grounded){
				myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, jumpForce);
			}
		} else {
			lastPositionX = transform.position.x;
		}
	}

	public void Jump(){
		if(grounded){
			myRigidBody.velocity = new Vector2 (myRigidBody.velocity.x, jumpForce);
		}
	}

    void OnTriggerEnter2D(Collider2D c)
    {
		/*
        if (c.gameObject.CompareTag("Killer"))
        {
            Debug.Log("Winter is coming");
            source.Pause();
            source.PlayOneShot(wolfDied);
            spriteRenderer.enabled = false;
            //yield return new WaitForSeconds(wolfDied.length);
            //source.Play();
            theGameManager.RestartPlayer();
        }*/
        Debug.Log(c.gameObject.tag);
        if (c.gameObject.CompareTag("OneWayPlatform"))
        {
            myCollider.isTrigger = true;
        }
    }
    void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("OneWayPlatform"))
        {
            myCollider.isTrigger = false;
        }
    }

}
