﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathColliderController : MonoBehaviour {

	public GameManager theGameManager;
	public PlayerController thePlayer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject.CompareTag ("Killer")) {
			Debug.Log ("Winter is coming");
			thePlayer.source.Pause ();
			thePlayer.source.PlayOneShot (thePlayer.wolfDied);
			//yield return new WaitForSeconds(wolfDied.length);
			//source.Play();
			theGameManager.RestartPlayer ();
		}
	}
}
