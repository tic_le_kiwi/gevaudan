﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCloudFast : MonoBehaviour {
    // Use this for initialization
    private float _maxX = 10F;
    private float _backX = -10F;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(0.005F, 0, 0);
        if (this.transform.position.x > _maxX)
        {
            this.transform.position = new Vector3(_backX, this.transform.position.y, 0);
        }
    }
}
