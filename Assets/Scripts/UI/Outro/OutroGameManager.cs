﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OutroGameManager : MonoBehaviour
{
    public GameObject theGround;
    public Transform groundGenerator;
    private Vector3 groundStartPoint;
    [SerializeField]
    private Camera mainCamera;

    public PlayerController thePlayer;
    private Vector3 playerStartPoint;

    [SerializeField]
    private GameObject text;

    // Use this for initialization
    void Awake()
    {
        groundStartPoint = groundGenerator.position;
        playerStartPoint = thePlayer.transform.position;
    }

    void GenerateFirstGround()
    {
        Instantiate(theGround, theGround.transform.position, theGround.transform.rotation);
    }

    void Update()
    {
        Debug.Log(text.transform.position.y);
        if (text.transform.position.y > 1000)
        {
            Debug.Log("here is come");
            ChangeLevel();
        }
    }

    void ChangeLevel()
    {
            mainCamera.GetComponent<CameraController>().setMoveCamera(false);
            //yield return new WaitForSeconds(3);
            SceneManager.LoadScene(2);
            Debug.Log("HEREEEEEE");
        }
}
