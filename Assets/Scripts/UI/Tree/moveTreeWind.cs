﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTreeWind : MonoBehaviour {

    private int _i = 0;
    private bool _right  = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (_right)
        {
            _i++;
            this.transform.Rotate(0, 0, 0.01F);
            if (_i > 100)
            {
                _right = false;
            }
        }
        else
        {
            _i--;
            this.transform.Rotate(0, 0, -0.01F);
            if (_i < -100)
            {
                _right = true;
            }
        }
    }
}
