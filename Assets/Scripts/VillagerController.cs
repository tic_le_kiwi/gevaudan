﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

public class VillagerController : MonoBehaviour
{

    public float moveSpeed;
    public AudioClip villagerDied;

    private Rigidbody2D myRigidBody;
    private GameManager gameManager;
    private float x;

	private SpriteRenderer mySpriteRenderer;
	private Animator myAnimator;

    private AudioSource source;
    public void Awake()
    {
        source = GetComponent<AudioSource>();
		mySpriteRenderer = GetComponent<SpriteRenderer> ();
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        GameObject gameControllerObject = GameObject.Find("GameManager");
        if (gameControllerObject != null)
        {
            gameManager = gameControllerObject.GetComponent<GameManager>();
        }
        if (gameManager == null)
        {
            Debug.Log("Cannot find 'GameManager' script");
        }
        x = transform.position.x;
    }

    // Use this for initialization
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        myRigidBody.velocity = new Vector2(moveSpeed, myRigidBody.velocity.y);
		myAnimator.SetFloat ("Speed", myRigidBody.velocity.x);
        if (transform.position.x > x + 5 && moveSpeed > 0)
        {
            Debug.Log(transform.position.x-x);
            Reverse();
        }
        else if (transform.position.x < x && moveSpeed<0)
        {
            Debug.Log(transform.position.x - x);
            Reverse();
        }
    }

    public void Reverse()
    {
        moveSpeed = -moveSpeed;
		mySpriteRenderer.flipX = !mySpriteRenderer.flipX;
    }

    public void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public IEnumerator OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            gameManager.AddScore(2);
            Debug.Log(source.clip.ToString());
            source.Pause();
            source.PlayOneShot(villagerDied);
            var collider = GetComponent<Collider2D>();
            Vector3 temp = GetComponent<Transform>().gameObject.transform.position;
            Debug.Log(temp.ToString());
            collider.isTrigger = true;
            yield return new WaitForSeconds(villagerDied.length);
            var renderer = GetComponent<SpriteRenderer>();
            renderer.enabled = false;
            Destroy(collider);
            
            Destroy(gameObject, villagerDied.length);
        }
    }

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            gameManager.AddScore(2);
            Destroy(gameObject);
        }
        
    }
}
