﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerPlatformGenerator : MonoBehaviour {

    public GameObject thePlatform;
    public GameObject theVillager;
    public GameObject generationPoint;
    public float positionXMax;
    public float positionXMin;
    public float positionYMax;
    public float positionYMin;

    private float platformWidth;


    // Use this for initialization
    void Awake()
    {
        platformWidth = thePlatform.GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        GeneratePlatform();
    }

    private void GeneratePlatform()
    {
        if (transform.position.x < generationPoint.transform.position.x)
        {
            var randomX = Random.Range(positionXMin, positionXMax);
            var randomY = Random.Range(positionYMin, positionYMax);
            float xPosition = transform.position.x + platformWidth + randomX*(2-(GameManager.score/124));
            float yPosition = generationPoint.transform.position.y + randomY;
            transform.position = new Vector3(xPosition, yPosition, transform.position.z);
            Instantiate(thePlatform, transform.position, transform.rotation);
			Instantiate(theVillager, transform.position + new Vector3(0.7f,0,0), transform.rotation);
        }
    }
}
