﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillagerTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit2D(Collider2D c)
    {
        VillagerController v = c.GetComponent<VillagerController>();
        if (v == null) return;
        v.Reverse();
    }

}
