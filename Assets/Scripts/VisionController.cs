﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionController : MonoBehaviour {

	private GameObject player;

	// Use this for initialization
	void Awake () {
		player = gameObject.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D c) {
		if (c.gameObject.tag.Contains("Platform") || c.gameObject.CompareTag("OneWayPlatform")) {
			// Jump my player
			player.GetComponent<PlayerController>().Jump();
		}
	}

}
