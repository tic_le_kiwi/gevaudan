﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfTrapController : MonoBehaviour {

	private Animator myAnimator;
	// Use this for initialization
	void Start () {
		myAnimator = GetComponent<Animator> ();
	}
	

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c.gameObject.CompareTag("Player"))
		{
			myAnimator.SetBool ("TargetOn", true);
		}
	}
}
